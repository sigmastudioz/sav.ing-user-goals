FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY target/user-goals-0.0.1-SNAPSHOT.jar user-goals-0.0.1-SNAPSHOT.jar
CMD ["java","-jar","/user-goals-0.0.1-SNAPSHOT.jar"]