package com.vfd.mfb.savi.goals.services;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

import com.google.cloud.ServiceOptions;
import com.google.cloud.pubsub.v1.AckReplyConsumer;
import com.google.cloud.pubsub.v1.MessageReceiver;
import com.google.cloud.pubsub.v1.Subscriber;
import com.google.common.reflect.TypeToken;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.gson.Gson;
import com.google.pubsub.v1.ProjectSubscriptionName;
import com.google.pubsub.v1.PubsubMessage;
import com.vfd.mfb.savi.goals.exception.GenericException;
import com.vfd.mfb.savi.goals.models.FrequencyInterestModel;
import com.vfd.mfb.savi.goals.models.FrequencyModel;
import com.vfd.mfb.savi.goals.models.GoalModel;
import com.vfd.mfb.savi.goals.models.InterestModel;
import com.vfd.mfb.savi.goals.repository.GoalRepository;
import com.vfd.mfb.savi.goals.repository.InterestRepository;
import com.vfd.mfb.savi.goals.utils.GoalHelper;
import com.vfd.mfb.savi.goals.utils.ShortHash;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.Date;

import javax.annotation.PostConstruct;

@Component
@Slf4j
public class InterestReceiver {

    @Value("${savi.google.pubsub.interest.projectId}")
    private String projectId;

    @Value("${savi.google.pubsub.interest.subscriptionId}")
    private String subscriptionId;

    private ProjectSubscriptionName subscriptionName;

    private Subscriber subscriber;

    private Type lisType;

    @Autowired
    private GoalHelper goalHelper;

    @Autowired
    private GoalRepository goalRepository;

    @Autowired
    private InterestRepository interestRepository;

    @Autowired
    private ShortHash shortHash;

    InterestReceiver () {
        this.lisType = new TypeToken<GoalModel>() {}.getType();
    }

    private static final BlockingQueue<PubsubMessage> messages = new LinkedBlockingDeque<>();
    class MessageReceiverProcess implements MessageReceiver {
        
        @Override
        public void receiveMessage(PubsubMessage message, AckReplyConsumer consumer) {
            messages.offer(message);
            consumer.ack();
        }
    }

    @Autowired
    public void startSubscriber() throws InterruptedException, ParseException, GenericException, IOException  {
        log.info("Starting pub/sub InterestReceiver");
        subscriptionName = ProjectSubscriptionName.of(projectId, subscriptionId);
        subscriber = null;

        try {
            subscriber = Subscriber.newBuilder(subscriptionName, new MessageReceiverProcess()).build();
            subscriber.addListener(
                new Subscriber.Listener() {
                    @Override
                    public void failed(Subscriber.State from, Throwable failure) {
                        // Handle failure. This is called when the Subscriber encountered a fatal error and is
                        // shutting down.
                        System.err.println(failure);
                    }
                },
                MoreExecutors.directExecutor()
            );
            subscriber.startAsync().awaitRunning();
            // while (true) {
            //     PubsubMessage message = messages.take();
            //     updateGoal(message);
            // } 

            
        } finally {
            // stop receiving messages
            if (subscriber != null) {
                subscriber.stopAsync().awaitTerminated();
            }
        }
    }

    private void updateGoal (PubsubMessage message) {
        System.out.println("Message Id: " + message.getMessageId());
        System.out.println("Data: " + message.getData().toStringUtf8());

        String goalId = message.getData().toStringUtf8().trim();
        GoalModel goalModel = goalRepository.findByGoalId(goalId);

        if (goalModel != null && !goalModel.isDeleted() && !goalModel.isGoalCompleted()
        ) {
            Long currentTime = System.currentTimeMillis();
            String frequencySelected = goalModel.getFrequencySelected();
            String interestId = shortHash.generateHash();
            FrequencyModel frequencyModel = goalHelper.getFrequency(goalModel.getStartDate(), goalModel.getEndDate(), frequencySelected);
            FrequencyInterestModel frequencyInterestModel = goalHelper.getInterestFrequency(frequencyModel, goalModel.getTotalPayment());
            goalModel.setTotalInterest(goalModel.getTotalInterest() + frequencyInterestModel.getNextScheduledInterestAmount());
            goalModel = goalHelper.updateInterest(goalModel, currentTime, frequencySelected);

            InterestModel interestModel = new InterestModel();
            interestModel.setGoalId(goalId);
            interestModel.setInterestDate(new Date(currentTime));
            interestModel.setUserId(goalModel.getUserId());
            interestModel.setTotalInterest(goalModel.getTotalInterest());
            interestModel.setPaymentId(goalModel.getPaymentId());
            interestModel.setTotalPayment(goalModel.getTotalPayment());
            interestModel.setInterestId(interestId);

            log.info(goalModel.toString());
            goalRepository.save(goalModel);
            interestRepository.save(interestModel);

            //Send Transaction Update
        } else {
            log.error("Goal not found with id " + goalId);
        }
    }
}