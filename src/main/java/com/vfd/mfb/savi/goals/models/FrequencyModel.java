package com.vfd.mfb.savi.goals.models;
import java.util.Date;

import lombok.Data;

@Data
public class FrequencyModel {
    private Date nextScheduledPaymentDate;
    private Long frequency;
    private Integer calFrequency;
}