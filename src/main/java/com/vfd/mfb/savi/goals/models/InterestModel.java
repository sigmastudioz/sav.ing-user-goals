package com.vfd.mfb.savi.goals.models;

import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@JsonIgnoreProperties(
    value = {"id", "createdAt", "updatedAt"},
    allowSetters = true
)
@Table(name = "interests", uniqueConstraints = {
    @UniqueConstraint(columnNames = {
        "interestId"
    })
})
public class InterestModel extends AuditModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    private Double totalInterest;

    private Double totalPayment;

    @Column(columnDefinition = "VARCHAR(50)", nullable = false)
    private String userId;

    @Column(columnDefinition = "VARCHAR(50) default null")
    private String paymentId;

    @Column(columnDefinition = "VARCHAR(50)", nullable = false)
    private String goalId;
    
    @Column(columnDefinition = "VARCHAR(50)", nullable = false)
    private String interestId;

    @Column(columnDefinition = "TIMESTAMP default null")
    private Date interestDate;
}