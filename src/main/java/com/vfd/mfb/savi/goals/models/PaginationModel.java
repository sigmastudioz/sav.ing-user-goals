package com.vfd.mfb.savi.goals.models;

import lombok.Data;

@Data
public class PaginationModel {
    private Integer page;
    private Integer limit;

    public PaginationModel () {
        this.page = 1;
        this.limit = 25;
    }
}