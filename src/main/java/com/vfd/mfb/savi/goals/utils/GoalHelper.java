package com.vfd.mfb.savi.goals.utils;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import javax.validation.constraints.NotBlank;

import com.vfd.mfb.savi.goals.exception.GenericException;
import com.vfd.mfb.savi.goals.models.FrequencyInterestModel;
import com.vfd.mfb.savi.goals.models.FrequencyModel;
import com.vfd.mfb.savi.goals.models.GoalModel;
import com.vfd.mfb.savi.goals.models.UserModel;
import com.vfd.mfb.savi.goals.repository.GoalRepository;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class GoalHelper {

    private Double interestRate;

    @Autowired
    private EmailHelper emailHelper;

    @Autowired
    private GoalRepository goalRepository;


    GoalHelper () {
        this.interestRate = 0.16;
    }

    public GoalModel updateGoal (GoalModel goalModel, GoalModel goal, Boolean payInterest) 
    throws ParseException, GenericException, IOException {
        Date startDate = goalModel.getStartDate();
        Date endDate = null;
        Boolean newEndDate = false;
        if (goal.getEndDate() != null) {
            endDate = goal.getEndDate();
            newEndDate = true;
        } else {
            endDate = goalModel.getEndDate();
        }

        String goalName = goal.getGoalName();
        String paymentId = goal.getPaymentId();
        String paymentType = goal.getPaymentType();
        Double totalPayment = goalModel.getTotalPayment();
        Double nextScheduledInterestAmount = null;
        Long currentTime = System.currentTimeMillis();

        if (goal.getTotalPayment() != null) {
            totalPayment = goal.getTotalPayment();
        }

        String frequencySelected = goalModel.getFrequencySelected();

        if (goal.getFrequencySelected() != null) {
            frequencySelected = goal.getFrequencySelected();
        }
        
        Double totalAmount = goalModel.getTotalAmount();

        if (goal.getTotalAmount() != null) {
            if (totalAmount < goal.getTotalAmount()) {
                throw new GenericException("You cannot reduce your current goal amount!");
            } else {
                goalModel.setTotalAmount(goal.getTotalAmount());
                totalAmount = goalModel.getTotalAmount();
            }
        }

        if (startDate != null && endDate != null) {
            if (newEndDate) {
                goalModel.setEndDate(checkEndDate(goalModel, endDate));
            }

            JSONObject frequencyAmount = calculateFrequencyAmount(startDate, endDate, frequencySelected, totalAmount);
            goalModel.setFrequencySelected(frequencySelected);
            goalModel.setFrequencyAmount(Double.valueOf(frequencyAmount.get("frequencyAmount").toString()));
            Date nextScheduledPaymentDate = new SimpleDateFormat("dd-MM-yyyy").parse(frequencyAmount.get("nextScheduledPaymentDate").toString());
            goalModel.setNextScheduledPaymentDate(nextScheduledPaymentDate);
            nextScheduledInterestAmount = Double.valueOf(frequencyAmount.get("nextScheduledInterestAmount").toString());
        }

        if (goalName != null) {
            goalModel.setGoalName(goalName);
        }


        if (paymentId != null) {
            goalModel.setPaymentId(paymentId);

            if (goalModel.getStatus() == null || goalModel.getStatus().equals("stopped")) {
                goalModel.setStatus("running");
            }
        }

        if (paymentType != null) {
            goalModel.setPaymentType(paymentType);
        }

        if (goal.getTotalPayment() != null) {
            goalModel.setTotalPayment(goalModel.getTotalPayment() + totalPayment);
        }

        Long goalTime = goalModel.getNextInterestDate().getTime();
        if (goal.getNextInterestDate() != null && goalModel.getFrequencySelected() != goal.getFrequencySelected()
            && goalTime < currentTime) {
            goalModel = updateInterest(goalModel, currentTime, goalModel.getFrequencySelected());
        }

        if (goalModel.getTotalPayment() >= goalModel.getTotalAmount()) {
            goalModel.setGoalCompleted(true);
            goalModel.setNextScheduledPaymentDate(null);
            goalModel.setStatus("completed");
        }

        goalModel = goalRepository.save(goalModel);

        return goalModel;
    }

    public FrequencyModel getFrequency(
        @NotBlank Date startDate, 
        @NotBlank Date endDate, 
        @NotBlank String frequencySelected
    ) {
        long frequency = 1;
        ZonedDateTime nextScheduledPaymentDate = null;
        ZonedDateTime startDateLocal = ZonedDateTime.ofInstant(startDate.toInstant(), ZoneId.of("UTC"));
        ZonedDateTime endDateLocal = ZonedDateTime.ofInstant(endDate.toInstant(), ZoneId.of("UTC"));

        Calendar cal = Calendar.getInstance();
        cal.setTime(Date.from(startDateLocal.toInstant()));
        int calFrequency = 1;

        switch (frequencySelected) {
            case "Q":
                calFrequency = 3;
                frequency = startDateLocal.until(endDateLocal, ChronoUnit.MONTHS) / 3;
                nextScheduledPaymentDate = startDateLocal.plusMonths(1 * 3);
                break;
            case "M":
                calFrequency = 12;
                frequency = startDateLocal.until(endDateLocal, ChronoUnit.MONTHS);
                nextScheduledPaymentDate = startDateLocal.plusMonths(1);
                break;
            case "W":
                calFrequency = cal.get(Calendar.WEEK_OF_YEAR);
                frequency = startDateLocal.until(endDateLocal, ChronoUnit.WEEKS);
                nextScheduledPaymentDate = startDateLocal.plusWeeks(1);
                break;
            default:
                calFrequency = cal.get(Calendar.DAY_OF_YEAR);
                frequency = startDateLocal.until(endDateLocal, ChronoUnit.DAYS);
                nextScheduledPaymentDate = startDateLocal.plusDays(1);
                break;
        }

        if (frequency == 0) {
            frequency = 1;
        }

        FrequencyModel data = new FrequencyModel();
        data.setFrequency(frequency);
        data.setCalFrequency(calFrequency);
        data.setNextScheduledPaymentDate(Date.from(nextScheduledPaymentDate.toInstant()));
        return data;
    }

    public FrequencyInterestModel getInterestFrequency(
        FrequencyModel frequencyModel,
        @NotBlank Double totalAmount
    ) {
        Long frequency = frequencyModel.getFrequency();
        Integer calFrequency =  frequencyModel.getCalFrequency();

        Double frequencyAmount = totalAmount/frequency;
        Double totalInterest = totalAmount * Math.pow(1 + interestRate/calFrequency, frequency);
        Double totalEstimatedGainedInterest = totalInterest - totalAmount;
        Double nextScheduledInterestAmount = totalEstimatedGainedInterest/frequency;

        FrequencyInterestModel frequencyInterestModel = new FrequencyInterestModel();
        frequencyInterestModel.setFrequencyAmount(frequencyAmount);
        frequencyInterestModel.setTotalInterest(totalInterest);
        frequencyInterestModel.setTotalEstimatedGainedInterest(totalEstimatedGainedInterest);
        frequencyInterestModel.setNextScheduledInterestAmount(nextScheduledInterestAmount);

        return frequencyInterestModel;
    }

    public JSONObject calculateFrequencyAmount(
        @NotBlank Date startDate, 
        @NotBlank Date endDate, 
        @NotBlank String frequencySelected, 
        @NotBlank Double totalAmount
    ) throws ParseException {
        FrequencyModel frequencyModel = getFrequency(startDate, endDate, frequencySelected);

        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        df.setTimeZone(TimeZone.getTimeZone("GMT"));

        Date nextPaymentDate = frequencyModel.getNextScheduledPaymentDate();
        Long frequency = frequencyModel.getFrequency();
        FrequencyInterestModel frequencyInterestModel = getInterestFrequency(frequencyModel, totalAmount);

        JSONObject data = new JSONObject();
        data.put("frequency", frequency);
        data.put("frequencyAmount", frequencyInterestModel.getFrequencyAmount());
        data.put("totalInterest", frequencyInterestModel.getTotalInterest());
        data.put("totalEstimatedGainedInterest", frequencyInterestModel.getTotalEstimatedGainedInterest());
        data.put("nextScheduledInterestAmount", frequencyInterestModel.getNextScheduledInterestAmount());
        data.put("nextScheduledPaymentDate", df.format(nextPaymentDate));
        data.put("startDate", df.format(startDate));
        data.put("endDate", df.format(endDate));
        return data;
    }

    public GoalModel updateInterest(GoalModel goalModel, Long currentTime, String frequencySelected) {
        Date now = new Date(currentTime);
        log.info("Updating Next Interest Date!");
        ZonedDateTime nextInterestDate = ZonedDateTime.ofInstant(now.toInstant(), ZoneId.of("UTC"));
        
        switch (frequencySelected) {
            case "Q":
                nextInterestDate = nextInterestDate.plusMonths(1 * 3);
                break;
            case "M":
                nextInterestDate = nextInterestDate.plusMonths(1);
                break;
            case "W":
                nextInterestDate = nextInterestDate.plusWeeks(1);
                break;
            default:
                nextInterestDate = nextInterestDate.plusDays(1);
                break;
        }
        goalModel.setNextInterestDate(Date.from(nextInterestDate.toInstant()));

        return goalModel;
    }

    public Date checkEndDate (GoalModel goalModel, Date endDate) throws GenericException {
        Date currentEndDate = goalModel.getEndDate();
        ZonedDateTime currentEndDateTime = ZonedDateTime.ofInstant(currentEndDate.toInstant(), ZoneId.of("UTC"));
        ZonedDateTime endDateTime = ZonedDateTime.ofInstant(endDate.toInstant(), ZoneId.of("UTC"));
        Long checkDateDiff = currentEndDateTime.until(endDateTime, ChronoUnit.DAYS);
        if (checkDateDiff < 0) {
            throw new GenericException("Your new date needs to be in the future.");
        }

        return endDate;
    }

    public Boolean validateDate (Date startDate, Date endDate) throws GenericException {
        Long currentTime = System.currentTimeMillis();
        Date now = new Date(currentTime);
        ZonedDateTime nowTime = ZonedDateTime.ofInstant(now.toInstant(), ZoneId.of("UTC"));
        ZonedDateTime startDateTime = ZonedDateTime.ofInstant(startDate.toInstant(), ZoneId.of("UTC"));
        ZonedDateTime endDateTime = ZonedDateTime.ofInstant(endDate.toInstant(), ZoneId.of("UTC"));
        Long checkDateDiff = startDateTime.until(endDateTime, ChronoUnit.DAYS);
        
        if (checkDateDiff < 0) {
            throw new GenericException("Your end date cannot be less than your start date. It needs to be either today or in the future.");
        }

        checkDateDiff = nowTime.until(startDateTime, ChronoUnit.DAYS);
        if (checkDateDiff < 0) {
            throw new GenericException("Your start date needs to be either today or in the future.");
        }

        checkDateDiff = nowTime.until(endDateTime, ChronoUnit.DAYS);
        if (checkDateDiff < 0) {
            throw new GenericException("Your end date needs to be either today or in the future.");
        }

        return true;
    }
}