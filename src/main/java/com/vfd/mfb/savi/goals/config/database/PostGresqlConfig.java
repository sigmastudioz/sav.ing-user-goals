package com.vfd.mfb.savi.goals.config.database;

import com.zaxxer.hikari.*;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import javax.sql.DataSource;

@Configuration
public class PostGresqlConfig {

  @Value("${spring.datasource.url}")
  private String dbUrl;
  @Value("${spring.datasource.username}")
  private String username;
  @Value("${spring.datasource.password}")
  private String password;

  @Bean
  public DataSource dataSource() {
      HikariConfig config = new HikariConfig();
      config.setJdbcUrl(dbUrl);
      config.setPassword(password);
      config.setUsername(username);
      return new HikariDataSource(config);
  }
}