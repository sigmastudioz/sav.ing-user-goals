package com.vfd.mfb.savi.goals.models;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class UserModel {
    private String firstName;
    private String lastName;
    private Long phoneNumber;
    private String userId;
    private String email;
    private String Message;
    private Integer ReturnCode;
}

