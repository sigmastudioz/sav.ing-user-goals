package com.vfd.mfb.savi.goals.repository;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import com.vfd.mfb.savi.goals.models.GoalModel;
import com.vfd.mfb.savi.goals.models.InterestModel;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface InterestRepository extends JpaRepository<InterestModel, Long> {

    InterestModel findByInterestId(String interestId); 

    @Query(
        value = "Select * FROM interests i "
        + "WHERE i.user_id= :userId AND i.goal_id= :goalId "
        + "ORDER BY created_at ASC LIMIT ALL",
        nativeQuery = true)
    List<InterestModel> findAllGoalInterestsByUser(@Param("userId") String userId, @Param("goalId") String goalId);

    @Query(
            value = "Select * FROM interests i "
            + "WHERE i.user_id = :userId AND i.goal_id= :goalId "
            + "AND created_at BETWEEN :startDate AND :endDate "
            + "ORDER BY created_at ASC LIMIT ALL",
            nativeQuery = true)
    List<InterestModel> findAllGoalInterestsRangeByUser(@Param("userId") String userId, @Param("goalId") String goalId, @Param("startDate") Timestamp startDate, @Param("endDate") Timestamp endDate);
}