package com.vfd.mfb.savi.goals;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
// import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
// @EnableEurekaClient
public class GoalsApplication {
	public static void main(String[] args) {
		SpringApplication.run(GoalsApplication.class, args);
	}
}
