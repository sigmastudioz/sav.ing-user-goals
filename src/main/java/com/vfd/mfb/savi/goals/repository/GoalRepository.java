package com.vfd.mfb.savi.goals.repository;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import com.vfd.mfb.savi.goals.models.GoalModel;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface GoalRepository extends JpaRepository<GoalModel, Long> {

    GoalModel findByGoalId(String goalId); 
    GoalModel findByPaymentId(String paymentId);

    @Query(
        value = "Select * FROM goals g "
        + "WHERE g.user_id= ?1 ORDER BY next_interest_date DESC, goal_completed ASC",
        nativeQuery = true)
    Page<GoalModel> findAllGoalsByUser(String userId, Pageable pageable);

    @Query(
        value = "Select * FROM goals g "
        + "WHERE g.user_id= ?1 AND g.deleted = false ORDER BY next_interest_date DESC, goal_completed ASC",
        nativeQuery = true)
    Page<GoalModel> findAllCurrentGoalsByUser(String userId, Pageable pageable);

    @Query(
            value = "Select * FROM goals g "
            + "WHERE g.deleted = false AND g.goal_completed = false AND g.status='running' "
            + "AND next_interest_date BETWEEN ?1 AND ?2 "
            + "OR g.deleted = false AND g.goal_completed = false AND g.status='running' AND next_interest_date = null "
            + "ORDER BY updated_at DESC LIMIT ALL",
            nativeQuery = true)
    List<GoalModel> findAllRunningGoals(Timestamp startDate, Timestamp endDate);

    @Query(
            value = "SELECT * FROM goals g WHERE " +
            "LOWER(g.goalName) LIKE LOWER(CONCAT('%',:searchTerm, '%')) AND g.deleted = false",
            nativeQuery = true)
    List<GoalModel> findByGoalName(@Param("searchTerm") String searchTerm, Pageable pageRequest);
}