package com.vfd.mfb.savi.goals.exception;

public class GenericException extends Exception {
    public GenericException(String message) { super(message); }
}