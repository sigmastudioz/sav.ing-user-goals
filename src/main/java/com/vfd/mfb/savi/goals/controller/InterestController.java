package com.vfd.mfb.savi.goals.controller;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.mashape.unirest.http.exceptions.UnirestException;
import com.vfd.mfb.savi.goals.exception.GenericException;
import com.vfd.mfb.savi.goals.exception.ResourceNotFoundException;
import com.vfd.mfb.savi.goals.models.GoalModel;
import com.vfd.mfb.savi.goals.models.InterestModel;
import com.vfd.mfb.savi.goals.models.UserModel;
import com.vfd.mfb.savi.goals.repository.GoalRepository;
import com.vfd.mfb.savi.goals.repository.InterestRepository;
import com.vfd.mfb.savi.goals.services.UserService;
import com.vfd.mfb.savi.goals.utils.ShortHash;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/goals/interests")
public class InterestController {
    @Autowired
    private GoalRepository goalRepository;

    @Autowired
    private InterestRepository interestRepository;

    @Autowired
    private ShortHash shortHash;
    
    @Autowired
    UserService userService;

    @GetMapping(value="/calculateDailyInterest", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GoalModel> addInterestToGoal(
        @RequestHeader (value="Authorization") String token,
        @RequestBody GoalModel goalModel
    )  throws ResourceNotFoundException {

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int totalDaysInYear = cal.getActualMaximum(Calendar.DAY_OF_YEAR);

        // UserModel userModel = userService.getUser(token);

        Double totalPayment = goalModel.getTotalPayment();
        Double totalInterest = goalModel.getTotalInterest();

        totalPayment = updateTotalPayment(0.16, totalPayment, totalDaysInYear);
        Double interestEarned = getTotalLifeTimeInterestEarned(totalPayment, totalInterest, totalDaysInYear);



        return new ResponseEntity<>(goalModel, HttpStatus.OK);
    }
    
    private Double updateTotalPayment(Double currentDailyInterest, Double totalPayment, int totalDaysInYear) {
        Double totalInterestEarned = (currentDailyInterest / totalDaysInYear) * totalPayment;
        totalPayment += totalInterestEarned;

        return totalPayment;
    }


    private Double getTotalLifeTimeInterestEarned(Double totalPayment, Double totalInterestEarned, int totalDaysInYear) {
        return (totalInterestEarned/totalPayment) * totalDaysInYear;
    }

    @GetMapping(value="/goal/{goalId}/{duration}", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<InterestModel>> getUserInterests(
        @RequestHeader(value="Authorization") String token,
        @PathVariable String goalId,
        @PathVariable String duration
    )  throws ResourceNotFoundException, ParseException, GenericException, UnirestException {

        UserModel userModel = userService.getUser(token);
        GoalModel goalModel = goalRepository.findByGoalId(goalId);
        String userId = userModel.getUserId();

        if (goalModel != null && userModel != null && userId.contains(goalModel.getUserId())) {
            LocalDate startLocalDate;
            LocalDate endLocalDate;
            LocalDate now = LocalDate.now();
            Date startDate;
            Date endDate;
            String dateNow;
            Calendar cal = Calendar.getInstance();
            List<InterestModel> interests;

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat sqlDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            LocalDateTime localDateTime; 

            switch(duration) {
                case "last-14-days":
                    startLocalDate = now.minusDays(14);
                    endLocalDate = now;
                    break;
                case "last-3-months":
                    startLocalDate = now.minusMonths(3);
                    endLocalDate = now;
                    break;
                case "last-6-months":
                    startLocalDate = now.minusMonths(6);
                    endLocalDate = now;
                    break;
                case "last-12-months":
                    startLocalDate = now.minusMonths(12);
                    endLocalDate = now;
                    break;
                case "this-week":
                    cal.set(Calendar.DAY_OF_WEEK, cal.getActualMinimum(Calendar.DAY_OF_WEEK));
                    localDateTime = new LocalDateTime(new Date(cal.getTimeInMillis()));
                    startLocalDate = localDateTime.toLocalDate();

                    cal.set(Calendar.DAY_OF_WEEK, cal.getActualMaximum(Calendar.DAY_OF_WEEK));
                    localDateTime = new LocalDateTime(new Date(cal.getTimeInMillis()));
                    endLocalDate = localDateTime.toLocalDate();
                    break;
                case "this-month":
                    cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
                    localDateTime = new LocalDateTime(new Date(cal.getTimeInMillis()));
                    startLocalDate = localDateTime.toLocalDate();

                    cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
                    localDateTime = new LocalDateTime(new Date(cal.getTimeInMillis()));
                    endLocalDate = localDateTime.toLocalDate();
                    break;
                case "last-month":
                    cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
                    localDateTime = new LocalDateTime(new Date(cal.getTimeInMillis()));
                    startLocalDate = localDateTime.toLocalDate().minusMonths(1);

                    cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
                    localDateTime = new LocalDateTime(new Date(cal.getTimeInMillis()));
                    endLocalDate = localDateTime.toLocalDate().minusMonths(1);
                    break;
                case "this-year":
                    cal.set(Calendar.MONTH, 0);
                    cal.set(Calendar.DAY_OF_MONTH, 1);    
                    localDateTime = new LocalDateTime(new Date(cal.getTimeInMillis()));
                    startLocalDate = localDateTime.toLocalDate();

                    cal.set(Calendar.MONTH, 11);
                    cal.set(Calendar.DAY_OF_MONTH, 31);
                    localDateTime = new LocalDateTime(new Date(cal.getTimeInMillis()));
                    endLocalDate = localDateTime.toLocalDate();
                    break;
                case "last-year":
                    cal.set(Calendar.MONTH, 0);
                    cal.set(Calendar.DAY_OF_MONTH, 1);
                    localDateTime = new LocalDateTime(new Date(cal.getTimeInMillis()));
                    startLocalDate = localDateTime.toLocalDate().minusYears(1);

                    cal.set(Calendar.MONTH, 11);
                    cal.set(Calendar.DAY_OF_MONTH, 31);
                    localDateTime = new LocalDateTime(new Date(cal.getTimeInMillis()));
                    endLocalDate = localDateTime.toLocalDate().minusYears(1);
                    break;
                default: //last-7-days
                    startLocalDate = now.minusDays(7);
                    endLocalDate = now;
                    break;
            }
            
            if (duration.equals("all-available")) {
                interests = interestRepository.findAllGoalInterestsByUser(userId, goalId);
            } else {
                startDate = sqlDate.parse(df.format(startLocalDate.toDate()).toString()+" 00:00:00.000");
                endDate = sqlDate.parse(df.format(endLocalDate.toDate()).toString()+" 23:59:59.999");

                ZonedDateTime startDateLocal = ZonedDateTime.ofInstant(startDate.toInstant(), ZoneId.of("UTC"));
                ZonedDateTime endDateLocal = ZonedDateTime.ofInstant(endDate.toInstant(), ZoneId.of("UTC"));

                Timestamp startTime = Timestamp.valueOf(startDateLocal.toLocalDateTime());
                Timestamp endTime = Timestamp.valueOf(endDateLocal.toLocalDateTime());
                interests = interestRepository.findAllGoalInterestsRangeByUser(userId, goalId, startTime, endTime);
            }

            return new ResponseEntity<>(interests, HttpStatus.OK);
        } else {
            throw new ResourceNotFoundException("Goal not found with id " + goalId);
        }
    }
}