package com.vfd.mfb.savi.goals.models;
import java.util.Date;

import lombok.Data;

@Data
public class FrequencyInterestModel {
    private Double frequencyAmount;
    private Double totalInterest;
    private Double totalEstimatedGainedInterest;
    private Double nextScheduledInterestAmount;
}