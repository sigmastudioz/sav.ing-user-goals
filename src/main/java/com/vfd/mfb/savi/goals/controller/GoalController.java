package com.vfd.mfb.savi.goals.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import com.mashape.unirest.http.exceptions.UnirestException;
import com.vfd.mfb.savi.goals.exception.GenericException;
import com.vfd.mfb.savi.goals.exception.ResourceNotFoundException;
import com.vfd.mfb.savi.goals.models.GoalModel;
import com.vfd.mfb.savi.goals.models.InterestModel;
import com.vfd.mfb.savi.goals.models.PaginationModel;
import com.vfd.mfb.savi.goals.models.UserModel;
import com.vfd.mfb.savi.goals.repository.GoalRepository;
import com.vfd.mfb.savi.goals.repository.InterestRepository;
import com.vfd.mfb.savi.goals.services.UserService;
import com.vfd.mfb.savi.goals.utils.EmailHelper;
import com.vfd.mfb.savi.goals.utils.GoalHelper;
import com.vfd.mfb.savi.goals.utils.ShortHash;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/goals")
public class GoalController {
    @Autowired
    private GoalRepository goalRepository;

    @Autowired
    private InterestRepository interestRepository;

    @Autowired
    private ShortHash shortHash;
    
    @Autowired
    UserService userService;

    @Autowired
    private EmailHelper emailHelper;

    @Autowired
    private GoalHelper goalHelper;

    @Value("${savi.config.interestTaskAuth}")
    private String interestTaskAuth;


    @GetMapping(value="/worker")
    public void getQueue(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String key = request.getParameter("key");

        // Do something with key.
        // [START_EXCLUDE]
        log.info("Worker is processing " + key);
    }

    @GetMapping(value="/goal/{goalId}", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GoalModel> getGoal(
        @RequestHeader (value="Authorization") String token,
        @PathVariable String goalId
    )  throws ResourceNotFoundException, UnirestException {
        UserModel userModel = userService.getUser(token);
        GoalModel goalModel = goalRepository.findByGoalId(goalId);

        if (userModel != null && userModel.getUserId() != null &&
            userModel.getUserId().contains(goalModel.getUserId())
        ) {
            return new ResponseEntity<>(goalModel, HttpStatus.OK);
        } else {
            throw new ResourceNotFoundException("Goal not found with id " + goalId);
        }
    }

    @GetMapping(value="/goal/payment/{paymentId}", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GoalModel> getGoalByPaymentId(
        @RequestHeader (value="Authorization") String token,
        @PathVariable String paymentId
    )  throws ResourceNotFoundException, UnirestException {
        UserModel userModel = userService.getUser(token);
        GoalModel goalModel = goalRepository.findByPaymentId(paymentId);

        if (userModel != null && userModel.getUserId() != null &&
            userModel.getUserId().contains(goalModel.getUserId())
        ) {
            return new ResponseEntity<>(goalModel, HttpStatus.OK);
        } else {
            throw new ResourceNotFoundException("Goal not found with payment id " + paymentId);
        }
    }

    @PostMapping(value="/create", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GoalModel> createGoal(
        @RequestHeader (value="Authorization") String token,
        @Valid @RequestBody GoalModel goalModel
    )  throws ResourceNotFoundException, ParseException, UnirestException, GenericException {
        UserModel userModel = userService.getUser(token);
        Date startDate = goalModel.getStartDate();
        Date endDate = goalModel.getEndDate();

        if (userModel != null && userModel.getUserId() != null && goalHelper.validateDate(startDate, endDate)) {
            Double totalAmount = goalModel.getTotalAmount();
            String frequencySelected = goalModel.getFrequencySelected();
            JSONObject frequency = goalHelper.calculateFrequencyAmount(startDate, endDate, frequencySelected, totalAmount);
            Double frequencyAmount = Double.valueOf(frequency.get("frequencyAmount").toString());
            if (goalModel.getFrequencyAmount() == null ||  frequencyAmount > goalModel.getFrequencyAmount()) {
                goalModel.setFrequencyAmount(frequencyAmount);
            }

            Long currentTime = System.currentTimeMillis();
            
            goalModel.setTotalInterest(0.0);
            goalModel.setTotalPayment(0.0);
            goalModel.setGoalId(shortHash.generateHash());
            goalModel.setUserId(userModel.getUserId());


            InterestModel interestModel = new InterestModel();
            interestModel.setGoalId(goalModel.getGoalId());
            interestModel.setInterestDate(new Date(currentTime));
            interestModel.setUserId(goalModel.getUserId());
            interestModel.setTotalInterest(goalModel.getTotalInterest());
            interestModel.setPaymentId(goalModel.getPaymentId());
            interestModel.setTotalPayment(goalModel.getTotalPayment());
            interestModel.setInterestId(shortHash.generateHash());
            
            goalModel = goalHelper.updateInterest(goalModel, currentTime, frequencySelected);

            goalModel = goalRepository.save(goalModel);
            interestRepository.save(interestModel);

            return new ResponseEntity<>(goalModel, HttpStatus.OK);
        } else {
            throw new ResourceNotFoundException("User not found!");
        }
    }

    @GetMapping("/")
    public String getGoalTest(){
        return "Testing Bitbucket Deployment - Goals";
    }

    @PostMapping(value="/", produces=MediaType.APPLICATION_JSON_VALUE)
    public Page<GoalModel> listGoals(
        @RequestHeader(value="Authorization") String token,
        @RequestBody PaginationModel pagination
    ) throws UnirestException {
        UserModel userModel = userService.getUser(token);
        if (userModel != null && userModel.getUserId() != null) {
            Integer page = pagination.getPage();
            if (page <= 0) {
                page = 1; 
            }

            Integer limit = pagination.getLimit();
            if (limit > 1000) {
                limit = 1000; 
            }

            if (limit <= 0) {
                limit = 25; 
            }

            page -= 1;
            Pageable pageable = PageRequest.of(page, limit);
            return goalRepository.findAllCurrentGoalsByUser(userModel.getUserId(), pageable);
        } else {
            throw new ResourceNotFoundException("Goals not found for user");
        }
    }

    @PostMapping(value="/user", produces=MediaType.APPLICATION_JSON_VALUE)
    public Page<GoalModel> listUserGoals(
        @RequestHeader(value="Authorization") String token,
        @RequestBody PaginationModel pagination
    ) throws UnirestException {
        UserModel userModel = userService.getUser(token);
        if (userModel != null && userModel.getUserId() != null) {
            Integer page = pagination.getPage();
            if (page <= 0) {
                page = 1; 
            }

            Integer limit = pagination.getLimit();
            if (limit > 1000) {
                limit = 1000; 
            }

            if (limit <= 0) {
                limit = 25; 
            }

            page -= 1;
            Pageable pageable = PageRequest.of(page, limit);
            return goalRepository.findAllGoalsByUser(userModel.getUserId(), pageable);
        } else {
            throw new ResourceNotFoundException("Goals not found for user");
        }
    }

    @PutMapping(value="/goal/{goalId}", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GoalModel> updateGoal(
        @RequestHeader(value="Authorization") String token,
        @PathVariable String goalId,
        @RequestBody GoalModel goal
    )  throws ResourceNotFoundException, ParseException, GenericException, UnirestException, IOException {
        UserModel userModel = userService.getUser(token);
        GoalModel goalModel = goalRepository.findByGoalId(goalId);

        if (goalModel != null && userModel != null 
            && userModel.getUserId().contains(goalModel.getUserId()) 
            && !goalModel.isDeleted() && !goalModel.isGoalCompleted()
        ) {
            goalModel = goalHelper.updateGoal(goalModel, goal, false);
            if (goalModel.getTotalPayment() >= goalModel.getTotalAmount()) {
                //SEND GO COMPLETED EMAIL
                Map emailData = new HashMap();
                emailData.put("name", userModel.getFirstName());
                emailData.put("signature", "Savi.ng");
                emailHelper.sendEmail("Savi.ng Goal Complete", userModel.getEmail(), "goal-complete", emailData);
            }
            return new ResponseEntity<>(goalModel, HttpStatus.OK);
        } else {
            throw new ResourceNotFoundException("Goal not found with id " + goalId);
        }
    }

    @DeleteMapping(value="/goal/{goalId}", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GoalModel> deleteGoal(
        @RequestHeader(value="Authorization") String token,
        @PathVariable String goalId
    )  throws ResourceNotFoundException, UnirestException {
        UserModel userModel = userService.getUser(token);
        GoalModel goalModel = goalRepository.findByGoalId(goalId);

        if (goalModel != null && userModel != null && userModel.getUserId().contains(goalModel.getUserId())) {
            long nowMillis = System.currentTimeMillis();
            Date now = new Date(nowMillis);
            goalModel.setDeleted(true);
            goalModel.setDeletedAt(now);
            goalModel = goalRepository.save(goalModel);

            return new ResponseEntity<>(goalModel, HttpStatus.OK);
        } else {
            throw new ResourceNotFoundException("Goal not found with id " + goalId);
        }
    }

    @GetMapping(value="/today", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<GoalModel>> getGoalsForToday(
        @RequestHeader (value="X-Task-Auth") String taskAuth
    )  throws ResourceNotFoundException, ParseException, GenericException {
        if (interestTaskAuth != null && taskAuth.equals(interestTaskAuth)) {
            Long currentTime = System.currentTimeMillis();
            Date now = new Date(currentTime);
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat sqlDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            String dateNow = df.format(now).toString();

            Date startDate = sqlDate.parse(dateNow+" 00:00:00.000");
            Date endDate = sqlDate.parse(dateNow+" 23:59:59.999");

            ZonedDateTime startDateLocal = ZonedDateTime.ofInstant(startDate.toInstant(), ZoneId.of("GMT")).plusDays(1);
            ZonedDateTime endDateLocal = ZonedDateTime.ofInstant(endDate.toInstant(), ZoneId.of("GMT")).plusDays(1);

            Timestamp startTime = Timestamp.valueOf(startDateLocal.toLocalDateTime());
            Timestamp endTime = Timestamp.valueOf(endDateLocal.toLocalDateTime());
            List<GoalModel> goals = goalRepository.findAllRunningGoals(startTime, endTime);

            return new ResponseEntity<>(goals, HttpStatus.OK);
        } else {
            throw new GenericException("You are not authorized to view this!");
        }
    }

    @PutMapping(value="/goal/{goalId}/update-interest", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GoalModel> getGoal(
        @RequestHeader (value="X-Task-Auth") String taskAuth,
        @RequestBody GoalModel goalRequestModel,
        @PathVariable String goalId
    ) throws GenericException { 
        GoalModel goalModel = goalRepository.findByGoalId(goalId);
        Long currentTime = System.currentTimeMillis();
        if (goalModel != null && interestTaskAuth != null && taskAuth.equals(interestTaskAuth)) {
            Long goalTime = goalModel.getNextInterestDate().getTime();
            if (goalTime >= currentTime && currentTime <= goalTime) {
                goalModel = goalHelper.updateInterest(goalModel, currentTime, goalModel.getFrequencySelected());
                goalModel.setTotalInterest(goalRequestModel.getTotalInterest());
                goalModel = goalRepository.save(goalModel);
            } else {
                log.info("No need to update Next Interest Date!");
            }
            return new ResponseEntity<>(goalModel, HttpStatus.OK);
        } else {
            throw new ResourceNotFoundException("Goal not found with id " + goalId);
        }
    }

    @PutMapping(value="/goal/{goalId}/pause", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GoalModel> pauseGoal(
        @RequestHeader(value="Authorization") String token,
        @PathVariable String goalId
    )  throws ResourceNotFoundException, UnirestException, IOException {
        UserModel userModel = userService.getUser(token);
        GoalModel goalModel = goalRepository.findByGoalId(goalId);

        if (goalModel != null && userModel != null 
            && userModel.getUserId().contains(goalModel.getUserId())
            && (goalModel.getStatus() == null || goalModel.getStatus().equals("running"))
        ) {
            goalModel.setStatus("paused");
            goalModel.setNextScheduledPaymentDate(null);
            goalModel = goalRepository.save(goalModel);
            
            //SEND GOAL PAUSED EMAIL
            Map emailData = new HashMap();
            emailData.put("name", userModel.getFirstName());
            emailData.put("signature", "Savi.ng");
            emailData.put("goalName", goalModel.getGoalName());
            emailHelper.sendEmail("Savi.ng Goal Paused", userModel.getEmail(), "goal-paused", emailData);
            return new ResponseEntity<>(goalModel, HttpStatus.OK);
        } else {
            throw new ResourceNotFoundException("Goal not found with id " + goalId);
        }
    }

    @PutMapping(value="/goal/{goalId}/resume", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GoalModel> resumeGoal(
        @RequestHeader(value="Authorization") String token,
        @PathVariable String goalId
    )  throws ResourceNotFoundException, UnirestException, ParseException, IOException { 
        UserModel userModel = userService.getUser(token);
        GoalModel goalModel = goalRepository.findByGoalId(goalId);

        if (goalModel != null && userModel != null 
            && userModel.getUserId().contains(goalModel.getUserId())
            && (goalModel.getStatus() == null || goalModel.getStatus().equals("paused"))
        ) {
            Long currentTime = System.currentTimeMillis();
            JSONObject frequencyAmount = goalHelper.calculateFrequencyAmount(goalModel.getStartDate(), goalModel.getEndDate(), goalModel.getFrequencySelected(), goalModel.getTotalAmount());
            Date nextScheduledPaymentDate = new SimpleDateFormat("dd-MM-yyyy").parse(frequencyAmount.get("nextScheduledPaymentDate").toString());
            goalModel.setNextScheduledPaymentDate(nextScheduledPaymentDate);
            goalModel.setStatus("running");
            Long goalTime = goalModel.getNextInterestDate().getTime();
            if (goalModel.getNextInterestDate() != null && goalTime < currentTime) {
                goalModel = goalHelper.updateInterest(goalModel, currentTime, goalModel.getFrequencySelected());
            }
            goalModel = goalRepository.save(goalModel);

            //SEND GOAL RESUMED EMAIL
            Map emailData = new HashMap();
            emailData.put("name", userModel.getFirstName());
            emailData.put("signature", "Savi.ng");
            emailData.put("goalName", goalModel.getGoalName());
            emailHelper.sendEmail("Savi.ng Goal Resumed", userModel.getEmail(), "goal-resumed", emailData);
            return new ResponseEntity<>(goalModel, HttpStatus.OK);
        } else {
            throw new ResourceNotFoundException("Goal not found with id " + goalId);
        }
    }

    @PutMapping(value="/goal/{goalId}/extend", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GoalModel> extendGoal(
        @RequestHeader(value="Authorization") String token,
        @PathVariable String goalId,
        @Valid @RequestBody GoalModel goal
    )  throws GenericException, UnirestException, ParseException, IOException {
        UserModel userModel = userService.getUser(token);
        GoalModel goalModel = goalRepository.findByGoalId(goalId);

        if (goalModel != null && userModel != null 
            && userModel.getUserId().contains(goalModel.getUserId())
            && (goalModel.isGoalCompleted())
        ) {
            Date startDate = goalModel.getStartDate();
            Date endDate = goal.getEndDate();
            Double totalAmount = goal.getTotalAmount();
            
            if (totalAmount <= goalModel.getTotalAmount()) {
                throw new GenericException("You need to increase your goal amount!");
            }

            if (totalAmount <= goalModel.getTotalPayment()) {
                throw new GenericException("Your new total amount has to be greater than your previous total payment");
            }

            Long currentTime = System.currentTimeMillis();
            Date now = new Date(currentTime);
            ZonedDateTime nowTime = ZonedDateTime.ofInstant(now.toInstant(), ZoneId.of("UTC"));
            ZonedDateTime endDateTime = ZonedDateTime.ofInstant(endDate.toInstant(), ZoneId.of("UTC"));
            Long checkDateDiff = nowTime.until(endDateTime, ChronoUnit.DAYS);
            if (checkDateDiff < 0) {
                throw new GenericException("Your end date needs to be either today or in the future.");
            }
            
            goalModel.setGoalCompleted(false);
            goalModel.setStatus("running");
            goalModel.setTotalAmount(totalAmount);
            goalModel.setEndDate(goalHelper.checkEndDate(goalModel, endDate));

            JSONObject frequencyAmount = goalHelper.calculateFrequencyAmount(startDate, endDate, goalModel.getFrequencySelected(), totalAmount);
            goalModel.setFrequencyAmount(Double.valueOf(frequencyAmount.get("frequencyAmount").toString()));
            Date nextScheduledPaymentDate = new SimpleDateFormat("dd-MM-yyyy").parse(frequencyAmount.get("nextScheduledPaymentDate").toString());
            goalModel.setNextScheduledPaymentDate(nextScheduledPaymentDate);

            //SEND GOAL EXTENDED EMAIL
            Map emailData = new HashMap();
            emailData.put("name", userModel.getFirstName());
            emailData.put("signature", "Savi.ng");
            emailData.put("goalName", goalModel.getGoalName());
            emailHelper.sendEmail("Savi.ng Goal Extended", userModel.getEmail(), "goal-extended", emailData);

            goalModel = goalRepository.save(goalModel);
            return new ResponseEntity<>(goalModel, HttpStatus.OK);
        } else {
            throw new ResourceNotFoundException("Goal not found with id " + goalId);
        }
    }

    @PutMapping(value="/goal/{goalId}/cash-out", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GoalModel> cashGoal(
        @RequestHeader(value="Authorization") String token,
        @PathVariable String goalId
    )  throws ResourceNotFoundException, UnirestException, ParseException, IOException { 
        UserModel userModel = userService.getUser(token);
        GoalModel goalModel = goalRepository.findByGoalId(goalId);

        if (goalModel != null && userModel != null 
            && userModel.getUserId().contains(goalModel.getUserId())
            && (goalModel.getStatus().equals("running"))
        ) {
            // Long currentTime = System.currentTimeMillis();
            // JSONObject frequencyAmount = goalHelper.calculateFrequencyAmount(goalModel.getStartDate(), goalModel.getEndDate(), goalModel.getFrequencySelected(), goalModel.getTotalAmount());
            // goalModel.setNextScheduledPaymentDate(null);
            // goalModel.setStatus("running");
            // goalModel = goalHelper.updateInterest(goalModel, currentTime, goalModel.getFrequencySelected());
            // goalModel = goalRepository.save(goalModel);
            return new ResponseEntity<>(goalModel, HttpStatus.OK);
        } else {
            throw new ResourceNotFoundException("Goal not found with id " + goalId);
        }
    }


    @PostMapping(value="/getfrequency", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getFrequencyAmount(
        @RequestHeader (value="Authorization") String token,
        @RequestBody GoalModel goal
    )  throws ResourceNotFoundException, ParseException, UnirestException {
        UserModel userModel = userService.getUser(token);

        if (userModel != null && userModel.getUserId() != null) {
            Date startDate = goal.getStartDate();
            Date endDate = goal.getEndDate();
            Double totalAmount = goal.getTotalAmount();
            String frequencySelected = goal.getFrequencySelected();
            JSONObject frequencyAmount = goalHelper.calculateFrequencyAmount(startDate, endDate, frequencySelected, totalAmount);
            JSONObject data = new JSONObject();
            data.put("frequencySelected", frequencySelected);
            data.put("frequency", frequencyAmount.get("frequency").toString());
            data.put("frequencyAmount", Double.valueOf(frequencyAmount.get("frequencyAmount").toString()));
            data.put("totalEstimatedGainedInterest", Double.valueOf(frequencyAmount.get("totalEstimatedGainedInterest").toString()));
            data.put("nextScheduledInterestAmount", Double.valueOf(frequencyAmount.get("nextScheduledInterestAmount").toString()));
            data.put("nextScheduledPaymentDate", frequencyAmount.get("nextScheduledPaymentDate").toString());
            data.put("startDate", frequencyAmount.get("startDate").toString()); 
            data.put("endDate", frequencyAmount.get("endDate").toString());
            data.put("totalInterest", frequencyAmount.get("totalInterest").toString());
            data.put("totalAmount", totalAmount);
            return new ResponseEntity<>(data.toString(), HttpStatus.OK);
        } else {
            throw new ResourceNotFoundException("User not found!");
        }
    }
}