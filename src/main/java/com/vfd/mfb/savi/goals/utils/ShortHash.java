package com.vfd.mfb.savi.goals.utils;

import java.util.Date;
import java.util.Random;

import org.hashids.Hashids;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ShortHash {
    @Value("{savi.config.salt}")
    private static String SaviSalt;

    public static String generateHash() {
        Long randId = generateRandom(5);
        Hashids hashids = new Hashids(SaviSalt + randId.toString());
        String hash = hashids.encode(new Date().getTime());
        return hash;
    }

    /*
     * Generates random 12 digit unique number
     * https://stackoverflow.com/questions/4655931/12-digit-unique-random-number-generation-in-java
     *
     * */
    public static Long generateRandom(int length) {
        Random random = new Random();
        char[] digits = new char[length];
        digits[0] = (char) (random.nextInt(9) + '1');
        for (int i = 1; i < length; i++) {
            digits[i] = (char) (random.nextInt(10) + '0');
        }
        return Long.parseLong(new String(digits));
    }

    public String getSaltString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }
}