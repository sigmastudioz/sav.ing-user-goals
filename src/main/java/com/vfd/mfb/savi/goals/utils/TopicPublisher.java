// package com.vfd.mfb.savi.goals.utils;

// import java.io.IOException;
// import java.util.ArrayList;
// import java.util.List;

// import javax.validation.Valid;

// import com.google.api.core.ApiFuture;
// import com.google.api.core.ApiFutures;
// import com.google.cloud.pubsub.v1.Publisher;
// import com.google.protobuf.ByteString;
// import com.google.pubsub.v1.ProjectTopicName;
// import com.google.pubsub.v1.PubsubMessage;

// import org.springframework.beans.factory.annotation.Value;
// import org.springframework.stereotype.Component;

// @Component
// class TopicPublisher {
//     @Value("${savi.google.pubsub.interest.projectId}")
//     private String projectId;

//     private ProjectTopicName topicName;
//     private Publisher publisher;
//     private List<ApiFuture<String>> futures;

//     TopicPublisher(@Valid String topicId) throws IOException, InterruptedException {
//         this.topicName = ProjectTopicName.of(projectId, topicId);
//         this.publisher = Publisher.newBuilder(topicName).build();
//         this.futures = new ArrayList<>();
//     }

//     private void publishMessage(String dataMsg) throws InterruptedException, Exception {
//         try {
//             ByteString data = ByteString.copyFromUtf8(dataMsg);
//             PubsubMessage pubsubMessage = PubsubMessage.newBuilder()
//                 .setData(data)
//                 .build();

//             ApiFuture<String> future = publisher.publish(pubsubMessage);
//             futures.add(future);
//         } finally {
//             // Wait on any pending requests
//             List<String> messageIds = ApiFutures.allAsList(futures).get();
        
//             for (String messageId : messageIds) {
//                 System.out.println(messageId);
//             }
        
//             if (publisher != null) {
//                 // When finished with the publisher, shutdown to free up resources.
//                 publisher.shutdown();
//             }
//         }
//     }
// }