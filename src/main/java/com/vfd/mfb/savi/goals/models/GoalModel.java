package com.vfd.mfb.savi.goals.models;

import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@JsonIgnoreProperties(
    value = {"id", "createdAt", "updatedAt"},
    allowSetters = true
)
@Table(name = "goals")
public class GoalModel extends AuditModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @NotBlank
    @Size(min = 3, max = 100)
    private String goalName;

    @NotNull
    private Double totalAmount;

    @Min(value = 0, message = "Total interest cannot be less than 0")
    @Max(value = 100, message = "Total interest should cannot be greater than 100")
    private Double totalInterest;

    private Double totalPayment;

    private Double frequencyAmount;

    @NotBlank
    @Column(columnDefinition = "VARCHAR(1)", nullable = false)
    private String frequencySelected;

    @NotNull
    @Column(columnDefinition = "DATE default null")
    @JsonFormat(pattern = "dd-MM-yyyy", timezone="GMT")
    private Date startDate;

    @NotNull
    @Column(columnDefinition = "DATE default null")
    @JsonFormat(pattern = "dd-MM-yyyy", timezone="GMT")
    private Date endDate;

    @Column(columnDefinition = "TIMESTAMP")
    private Date nextScheduledPaymentDate;

    @Column(columnDefinition = "TIMESTAMP default null")
    private Date nextInterestDate;

    @Column(columnDefinition = "VARCHAR(50)", nullable = false)
    private String userId;

    @Column(columnDefinition = "VARCHAR(50) default null")
    private String paymentId;

    @Column(columnDefinition = "VARCHAR(4) default null")
    private String paymentType;

    @Column(columnDefinition = "VARCHAR(50)", nullable = false)
    private String goalId;

    @Column(columnDefinition = "BOOLEAN default false")
    private boolean goalCompleted;

    @Column(columnDefinition = "BOOLEAN default false")
    private boolean deleted;

    @Column(name = "deleted_at", columnDefinition = "TIMESTAMP")
    private Date deletedAt;

    @Column(columnDefinition = "VARCHAR(10) default 'stopped'")
    private String status;
    
}