package com.vfd.mfb.savi.goals.services;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.vfd.mfb.savi.goals.exception.ResourceNotFoundException;
import com.vfd.mfb.savi.goals.models.UserModel;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserService {
    private String url;

    @Value("${savi.config.auth.baseUrl}")
    private String baseUrl;

    private Type lisType;

    public UserService (){
        this.lisType = new TypeToken<UserModel>() {}.getType();
    }

    public UserModel getUser(String token) throws UnirestException {
        Map headers = new HashMap();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", token);

        HttpResponse<JsonNode> response = Unirest.post(baseUrl+"/users/user")
            .headers(headers)
            .asJson();
       
        JSONObject userData = processResponse(response);
        UserModel userModel = new Gson().fromJson(userData.toString(), lisType);
        
        if (userModel != null && userModel.getUserId() != null) {
            return userModel;
        } else {
            throw new ResourceNotFoundException(userModel.getMessage());
        }
    }

    public UserModel updateUser(String token, String userId, JSONObject data) throws UnirestException {
        Map headers = new HashMap();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", token);
        
        HttpResponse<JsonNode> response =  Unirest.put(baseUrl+"/users/user/"+userId)
            .headers(headers)
            .body(data)
            .asJson();

        JSONObject userData = processResponse(response);
        UserModel userModel = new Gson().fromJson(userData.toString(), lisType);
        
        if (userModel != null && userModel.getUserId() != null) {
            return userModel;
        } else {
            throw new ResourceNotFoundException(userModel.getMessage());
        }
    }

    private JSONObject processResponse(HttpResponse<JsonNode> response) {
        JsonNode jsonNode = response.getBody();
        JSONObject responseObject = jsonNode.getObject();

        return responseObject;
    }
}